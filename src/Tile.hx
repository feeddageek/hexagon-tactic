import h3d.col.Point;
import h3d.scene.*;
import shapes.Honeycomb;

class Tile extends Mesh{
    public static inline var RADIUS = 1.;

    public function new(mat, s3d) {
        var prim = new Honeycomb(new Point(0,0,2*RADIUS), RADIUS, 2*RADIUS);
        prim.unindex();
        prim.addNormals();
        prim.addUVs();
        super(prim, mat, s3d);
    }
}
