import h3d.col.Point;
import h3d.scene.*;
import hxd.Res;
import shapes.Honeycomb;


class Main extends hxd.App {
    
    private static inline var SQRT_OF_3 = 1.73205080757;

    var shadow: h3d.pass.ShadowMap;
    var time : Float = 0.;
    var dir : h3d.scene.DirLight;

    var center : Mesh;
    var obj : Array<Mesh>;


    override function init() {
        // Floor
        var floor = new h3d.prim.Cube(10, 10, 0.1);
        floor.addNormals();
        floor.translate( -5, -5, 0);
        var m = new h3d.scene.Mesh(floor, s3d);
        m.material.color.setColor(0x402800);

        var ground = h3d.mat.Material.create(hxd.Res.ground.toTexture());
        ground.texture.filter = Nearest;

        center = new Tile(ground, s3d);
        obj = [
            new Tile(ground, s3d),
            new Tile(ground, s3d),
            new Tile(ground, s3d),
            new Tile(ground, s3d),
            new Tile(ground, s3d),
            new Tile(ground, s3d),
        ];

        // Tile positionning
        obj[0].y = -SQRT_OF_3 * Tile.RADIUS;
        obj[0].z = -.1;

        obj[1].y = -SQRT_OF_3 * Tile.RADIUS/2.;
        obj[1].x = Tile.RADIUS*3./2.;
        obj[1].z = -.2;

        obj[2].y = SQRT_OF_3 * Tile.RADIUS/2.;
        obj[2].x = Tile.RADIUS*3./2.;
        obj[2].z = -1.24;

        obj[3].y = SQRT_OF_3 * Tile.RADIUS;
        obj[3].z = -1.5;

        obj[4].y = SQRT_OF_3 * Tile.RADIUS/2.;
        obj[4].x = -Tile.RADIUS*3./2.;
        obj[4].z = -1.1;

        obj[5].y = -SQRT_OF_3 * Tile.RADIUS/2.;
        obj[5].x = -Tile.RADIUS*3./2.;
        obj[5].z = -.5;


        dir = new h3d.scene.DirLight(new h3d.Vector(-0.3, -0.2, -1), s3d);
        s3d.lightSystem.ambientLight.setColor(0x707070);

        shadow = s3d.renderer.getPass(h3d.pass.ShadowMap);
        shadow.size = 2048;
        shadow.power = 200;
        shadow.blur.radius= 0;
        shadow.bias *= 0.1;
        shadow.color.set(0.7, 0.7, 0.7);

        s3d.camera.pos.set(12, 12, 6);
        s3d.camera.zNear = 6;
        s3d.camera.zFar = 30;
        new h3d.scene.CameraController(s3d).loadFromCamera();
    }

    override function update( dt : Float ) {
        time += dt * 0.01;
        dir.setDirection(new h3d.Vector(Math.cos(time), Math.sin(time), -1));
    }

    static function main() {
        hxd.Res.initEmbed();
        new Main();
    }

}
