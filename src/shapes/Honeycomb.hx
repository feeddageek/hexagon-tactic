package shapes;
import macros.MRegularPrism;
import h3d.prim.UV;
import hxd.impl.UInt16;
import h3d.prim.Polygon;
import h3d.col.Point;

class Honeycomb extends Polygon {

    public function new (center: Point, radius: Float, height: Float) {
        var p = [];
        for (i in 0...6) {
            p.push(corner(center, radius, i, 0));
            p.push(corner(center, radius, i, height));
        }

        var idx = new hxd.IndexBuffer();
        MRegularPrism.pushFacesIndex(idx, 6, false);
        super(p, idx);
        }

        override function addUVs() {
        unindex();

        var z = [new UV(0, 1), new UV(0.125, 1), new UV(0.25, 1), new UV(0.375, 1), new UV(0.5, 1), new UV(0.625, 1)];
        var x = [new UV(0.125, 1), new UV(0.25, 1), new UV(0.375, 1), new UV(0.5, 1), new UV(0.625, 1), new UV(0.75, 1)];
        var y = [new UV(0, 0), new UV(0.125, 0), new UV(0.25, 0), new UV(0.375, 0), new UV(0.5, 0), new UV(0.625, 0)];
        var o = [new UV(0.125, 0), new UV(0.25, 0), new UV(0.375, 0), new UV(0.5, 0), new UV(0.625, 0), new UV(0.75, 0)];

        var t = [Honeycomb.UV(0), Honeycomb.UV(1), Honeycomb.UV(2), Honeycomb.UV(3), Honeycomb.UV(4), Honeycomb.UV(5)];
        var indexes = [0,1,2,3,4,5];
        for (i in 0...4) {
            var j = i+Std.random(5-i);
            if (i!=j){
                var temp = indexes[i];
                indexes[i] = indexes[j];
                indexes[j] = temp;
            }
        }
        uvs = [
            x[indexes[0]], z[indexes[0]], y[indexes[0]],
            x[indexes[0]], y[indexes[0]], o[indexes[0]],
            x[indexes[1]], z[indexes[1]], y[indexes[1]],
            x[indexes[1]], y[indexes[1]], o[indexes[1]],
            x[indexes[2]], z[indexes[2]], y[indexes[2]],
            x[indexes[2]], y[indexes[2]], o[indexes[2]],
            x[indexes[3]], z[indexes[3]], y[indexes[3]],
            x[indexes[3]], y[indexes[3]], o[indexes[3]],
            x[indexes[4]], z[indexes[4]], y[indexes[4]],
            x[indexes[4]], y[indexes[4]], o[indexes[4]],
            x[indexes[5]], z[indexes[5]], y[indexes[5]],
            x[indexes[5]], y[indexes[5]], o[indexes[5]],

            t[0], t[1], t[2],
            t[0], t[2], t[3],
            t[0], t[3], t[4],
            t[0], t[4], t[5]
        ];
    }

    private static inline function circlePointX(centerX: Float, radius: Float, angle: Float){
        return centerX + radius * Math.cos(angle);
    }

    private static inline function circlePointY(centerY: Float, radius: Float, angle: Float){
        return centerY + radius * Math.sin(angle);
    }

    private static function corner(center: Point, radius: Float, i: UInt16, z: Float) {
        var angle = angles[i%6];
    return new Point(
        circlePointX(center.x, radius, angle),
        circlePointY(center.y, radius, angle),
        z);
    }

    private static function UV(i: UInt16) {
        return new UV(circlePointX(.875, .125, i), circlePointY(.5,.5,i));
    }

    private static var angles = [0, Math.PI/3., 2.*Math.PI/3., Math.PI, 4.*Math.PI/3, 5.*Math.PI/3];
}
