package macros;
import h3d.prim.UV;
import hxd.impl.UInt16;
import haxe.macro.Expr.ExprOf;

class MRegularPrism {

    public static macro function pushFacesIndex(idx: ExprOf<hxd.IndexBuffer>, faces: UInt16, doubleSide: Bool = false) {
        var expres = [];
        for (i in 0...faces ) {
            var a = i * 2;
            var b = (a + 2) % (faces * 2);
            var c = b + 1;
            var d = a + 1;
            expres.push(macro {
                $idx.push($v{a});$idx.push($v{b});$idx.push($v{c});
                $idx.push($v{a});$idx.push($v{c});$idx.push($v{d});
            });
            if (doubleSide) {
                expres.push(macro {
                    $idx.push($v{b}); $idx.push($v{d}); $idx.push($v{c});
                    $idx.push($v{b}); $idx.push($v{i}); $idx.push($v{d});
                });
            }
        }
        for (i in 1...faces-1) {
            expres.push(macro {$idx.push(1);$idx.push($v{1+2*i});$idx.push($v{3+2*i});});
            if (doubleSide) expres.push(macro {$idx.push(1);$idx.push($v{3+2*i});$idx.push($v{1+2*i});});
        }
        return macro{
            $b{expres};}
    }

    public static macro function buildUVs(uvs, faces: UInt16, doubleSide: Bool = false) {
        var expres = [];
        var z = [new UV(0, 1), new UV(0.125, 1), new UV(0.25, 1), new UV(0.375, 1), new UV(0.5, 1), new UV(0.625, 1)];
        var x = [new UV(0.125, 1), new UV(0.25, 1), new UV(0.375, 1), new UV(0.5, 1), new UV(0.625, 1), new UV(0.75, 1)];
        var y = [new UV(0, 0), new UV(0.125, 0), new UV(0.25, 0), new UV(0.375, 0), new UV(0.5, 0), new UV(0.625, 0)];
        var o = [new UV(0.125, 0), new UV(0.25, 0), new UV(0.375, 0), new UV(0.5, 0), new UV(0.625, 0), new UV(0.75, 0)];
        var zval = [for(i in 0...faces) macro new UV($v{0.125*i}, 1)];
        expres.push(macro $uvs = $a{zval});
        return macro $b{expres};
    }
}
